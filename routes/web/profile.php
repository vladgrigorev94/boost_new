<?php

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');
Route::get('/home/inprogress', [App\Http\Controllers\HomeController::class, 'inprogress'])
    ->name('inprogress');
Route::get('/home/endorders', [App\Http\Controllers\HomeController::class, 'endorders'])
    ->name('endorders');
Route::get('/home/transactions', [App\Http\Controllers\HomeController::class, 'transactions'])
    ->name('transactions');
Route::get('/home/paymentrequests', [App\Http\Controllers\HomeController::class, 'paymentrequests'])
    ->name('paymentrequests');
Route::get('/order/{id}', [App\Http\Controllers\HomeController::class, 'showOrder'])
    ->name('order.show');
Route::post('/order/take', [App\Http\Controllers\HomeController::class, 'takeOrder'])
    ->name('order.take');
Route::post('/order/end', [App\Http\Controllers\HomeController::class, 'endOrder'])
    ->name('order.end');
Route::post('/order/broke', [App\Http\Controllers\HomeController::class, 'brokeOrder'])
    ->name('order.broke');
Route::post('/message/add', [App\Http\Controllers\HomeController::class, 'addMessage'])
    ->name('message.add');

Route::post('/home/edit', [App\Http\Controllers\HomeController::class, 'editProfileHandler'])
    ->name('profile.edit');

Route::post('/home/withdrawal', [App\Http\Controllers\HomeController::class, 'withdrawalHandler'])
    ->name('withdrawal.handler');

Route::post('/home/progress', [App\Http\Controllers\HomeController::class, 'progressHandler'])
    ->name('progress.handler');
