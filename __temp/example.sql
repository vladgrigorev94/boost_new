-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Апр 27 2021 г., 17:40
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
set sql_require_primary_key = 0;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `exchange`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blocks`
--

CREATE TABLE `blocks` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blocks`
--

INSERT INTO `blocks` (`id`, `title`, `content`, `thumb`, `created_at`, `updated_at`) VALUES
(1, 'VPN PROTECTION', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same location before accessing an account.', '/assets/images/minigun.png', '2021-04-20 10:44:17', '2021-04-25 18:28:54'),
(2, 'ORDER TRACKING', 'Our platform provides order tracking and scheduling for all users. When you submit a boosting order, it creates a private section for you in the member\'s area. In your order, you can follow up on the progress made by the booster and also schedule boosting hours in the chat. In your order, you can follow', '/assets/images/minigun.png', '2021-04-25 18:29:07', '2021-04-25 18:29:07'),
(3, 'VPN PROTECTION 2', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same location before accessing an account.', '/assets/images/minigun.png', '2021-04-25 18:29:27', '2021-04-25 18:30:31'),
(4, 'ORDER TRACKING 2', 'Our platform provides order tracking and scheduling for all users. When you submit a boosting order, it creates a private section for you in the member\'s area. In your order, you can follow up on the progress made by the booster and also schedule boosting hours in the chat. In your order, you can follow', '/assets/images/minigun.png', '2021-04-25 18:29:35', '2021-04-25 18:30:36'),
(5, 'VPN PROTECTION 3', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same', '/assets/images/minigun.png', '2021-04-25 18:30:48', '2021-04-25 18:30:48'),
(6, 'VPN PROTECTION 4', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same', '/assets/images/minigun.png', '2021-04-25 18:30:59', '2021-04-25 18:30:59'),
(7, 'VPN PROTECTION 5', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same', '/assets/images/minigun.png', '2021-04-25 18:31:11', '2021-04-25 18:31:11'),
(8, 'VPN PROTECTION 6', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same', '/assets/images/minigun.png', '2021-04-25 18:31:22', '2021-04-25 18:31:22'),
(9, 'VPN PROTECTION 7', 'BuyBoosting\'s boosters utilize VPN services before connecting to the user\'s account. The measure helps us to operate in maximum safety. Our platform recognizes the country of the buyer, and the boosters use the information to set their VPNs to the same', '/assets/images/minigun.png', '2021-04-25 18:31:34', '2021-04-25 18:31:34'),
(10, 'Job', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:31:51', '2021-04-25 18:31:51'),
(11, 'Location', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:32:22', '2021-04-25 18:32:22'),
(12, 'Customer service', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:32:34', '2021-04-25 18:32:34'),
(13, 'Power', 'Go to check-out and add funds to purchase the\r\n(PayPal, Visa, Mastercard, Wechat, Bitcoin and more)', '/assets/images/minigun.png', '2021-04-25 18:32:45', '2021-04-25 18:32:45'),
(14, 'Create', 'Go to check-out and add funds to purchase the\r\n(PayPal, Visa, Mastercard, Wechat, Bitcoin and more)', '/assets/images/minigun.png', '2021-04-25 18:32:57', '2021-04-25 18:32:57'),
(15, 'Boost', 'Go to check-out and add funds to purchase the\r\n(PayPal, Visa, Mastercard, Wechat, Bitcoin and more)', '/assets/images/minigun.png', '2021-04-25 18:33:09', '2021-04-25 18:33:09'),
(16, 'Job 2', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:33:24', '2021-04-25 18:33:59'),
(17, 'Location 2', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:33:39', '2021-04-25 18:33:54'),
(18, 'Customer service 2', 'Most definitely. Our boosters have been working with,\r\nyears have very strict instructions to look out for our', '/assets/images/minigun.png', '2021-04-25 18:33:48', '2021-04-25 18:33:48'),
(19, 'Which games do ?', 'boosting.gg boosting service focused on the most popular online games like Dota 2 League.', '/assets/images/minigun.png', '2021-04-25 18:34:21', '2021-04-25 18:34:21'),
(20, 'Step 1', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10'),
(21, 'Step 2', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10'),
(22, 'Step 3', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10'),
(23, 'Step 4', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10'),
(24, 'Step 5', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10'),
(25, 'Step 6', 'Step 1', NULL, '2021-04-26 21:23:10', '2021-04-26 21:23:10');

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_h1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `game_id` bigint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`id`, `slug`, `meta_title`, `meta_description`, `meta_h1`, `thumb`, `content`, `created_at`, `updated_at`, `game_id`) VALUES
(1, 'blog', 'blog', 'blog', 'blog', 'images/uploads/c1b3d106084781b89774e0c49553f3d1.jpg', 'blog', '2021-04-20 11:21:50', '2021-04-25 20:39:00', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Александр Шибистый', 'shibisty.a.wezom@gmail.com', '321', '2021-04-20 10:49:39', '2021-04-20 10:49:39'),
(2, 'Александр Шибистый', 'shibisty.a.wezom@gmail.com', 'dsasd', '2021-04-20 12:56:46', '2021-04-20 12:56:46'),
(3, 'Александр Шибистый', 'shibisty.a.wezom@gmail.com2', 'sadasdasdasd', '2021-04-20 12:57:04', '2021-04-20 12:57:04');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `animation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`id`, `name`, `slug`, `thumb`, `text`, `created_at`, `updated_at`, `animation`, `image`) VALUES
(1, 'Apex Legend', 'apex-legend', 'images/uploads/3354cf84b3b9e54108c5d1cf485537ca.png', 'Apex Legend', '2021-04-20 10:46:42', '2021-04-20 13:18:40', '/assets/images/game-bg/apex1.gif', '/assets/images/game-bg/apexlarge.png'),
(2, 'Velorant', 'velorant', 'images/uploads/f3df049d1f9efe926b9cf5d23f5f5e8a.png', 'Velorant', '2021-04-20 13:19:02', '2021-04-20 13:19:02', '/assets/images/game-bg/apex1.gif', '/assets/images/game-bg/apexlarge.png'),
(3, 'Dota 2', 'dota-2', 'images/uploads/71941a85c6081a0011138b53df1e7b8d.png', 'Dota 2', '2021-04-20 13:19:28', '2021-04-20 13:19:28', '/assets/images/game-bg/apex1.gif', '/assets/images/game-bg/apexlarge.png'),
(4, 'Call of Duty: Warzone', 'call-of-duty-warzone', 'images/uploads/0f7efb1b21ed89d179b11b9d6d9d67c4.png', 'Call of Duty: Warzone', '2021-04-20 13:20:00', '2021-04-20 13:20:00', '/assets/images/game-bg/apex1.gif', '/assets/images/game-bg/apexlarge.png');

-- --------------------------------------------------------

--
-- Структура таблицы `metas`
--

CREATE TABLE `metas` (
  `id` bigint UNSIGNED NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_h1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `metas`
--

INSERT INTO `metas` (`id`, `uri`, `meta_title`, `meta_description`, `meta_h1`, `thumb`, `created_at`, `updated_at`) VALUES
(1, '/', 'Home page', 'Home page', 'Home page', 'images/uploads/36da3505c6a7b1a2130563074b42ead2.jpg', '2021-04-20 10:33:47', '2021-04-20 10:33:52');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_04_12_122428_add_role_to_users', 2),
(5, '2021_04_19_231752_metas', 3),
(6, '2021_04_19_231825_pages', 3),
(7, '2021_04_19_231856_settings', 3),
(8, '2021_04_19_232801_blocks', 3),
(9, '2021_04_19_233040_contacts', 3),
(10, '2021_04_19_233740_games', 3),
(11, '2021_04_20_134702_create_blogs', 4),
(12, '2021_04_25_213630_create_social', 5),
(13, '2021_04_25_213647_create_reviews', 5),
(14, '2021_04_25_214000_create_tarifs', 5),
(15, '2021_04_25_230215_add_game_id_to_blogs', 5),
(16, '2021_04_26_001216_add_animation_to_games', 6),
(17, '2021_04_26_002632_create_orders', 7),
(18, '2021_04_26_233306_add_image_to_games', 7),
(19, '2021_04_27_003916_create_option_group', 8),
(20, '2021_04_27_003927_create_options', 8),
(21, '2021_04_27_004142_create_orders_options', 8),
(22, '2021_04_27_014023_add_max_rank_to_tarifs', 9),
(23, '2021_04_27_015753_add_max_rating_to_orders', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE `options` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 'Test', 1, '2021-04-26 22:18:20', '2021-04-26 22:18:20'),
(2, 'Test 2', 1, '2021-04-26 22:19:40', '2021-04-26 22:19:46'),
(3, 'test 3', 2, '2021-04-26 23:08:35', '2021-04-26 23:08:35');

-- --------------------------------------------------------

--
-- Структура таблицы `option_groups`
--

CREATE TABLE `option_groups` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarif_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `option_groups`
--

INSERT INTO `option_groups` (`id`, `name`, `tarif_id`, `created_at`, `updated_at`) VALUES
(1, 'Free', 1, '2021-04-26 22:15:35', '2021-04-26 22:15:35'),
(2, 'Test 2', 2, '2021-04-26 22:18:59', '2021-04-26 22:18:59');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` bigint UNSIGNED NOT NULL,
  `tarif_id` bigint UNSIGNED DEFAULT NULL,
  `client_id` bigint UNSIGNED DEFAULT NULL,
  `prof_id` bigint UNSIGNED DEFAULT NULL,
  `amount` decimal(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `status` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `max_rating` bigint UNSIGNED DEFAULT '0',
  `min_rating` bigint UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `tarif_id`, `client_id`, `prof_id`, `amount`, `status`, `created_at`, `updated_at`, `max_rating`, `min_rating`) VALUES
(1, 1, 1, NULL, '0.00', 0, '2021-04-26 23:01:16', '2021-04-26 23:01:16', 100, 0),
(2, 1, 1, NULL, '0.00', 0, '2021-04-26 23:03:57', '2021-04-26 23:03:57', 100, 0),
(3, 1, 1, NULL, '0.00', 0, '2021-04-26 23:04:50', '2021-04-26 23:04:50', 100, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `orders_options`
--

CREATE TABLE `orders_options` (
  `id` bigint UNSIGNED NOT NULL,
  `order_id` bigint UNSIGNED NOT NULL,
  `option_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders_options`
--

INSERT INTO `orders_options` (`id`, `order_id`, `option_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2021-04-26 23:03:57', NULL),
(2, 2, 2, '2021-04-26 23:03:57', NULL),
(3, 3, 1, '2021-04-26 23:04:50', NULL),
(4, 3, 2, '2021-04-26 23:04:50', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` bigint UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_h1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `slug`, `meta_title`, `meta_description`, `meta_h1`, `thumb`, `content`, `created_at`, `updated_at`) VALUES
(1, 'loyalityprogram', 'Loyalty Program', 'Loyalty Program', 'Loyalty Program', 'images/uploads/db9830929a374171fccba1b9f3c1cf62.jpg', '<gameslide class=\"gameslide\">\r\n\r\n        <div class=\"gameslide-aling\">\r\n            <div class=\"gameslide-block\">\r\n                <img src=\"/assets/images/game-bg/apexlarge.png\" class=\"game-large\">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"features-title-2\">\r\n            <div class=\"features-title-aling\">\r\n                <div class=\"features-title-slog\" style=\"margin-left: 587px!important;\">\r\n                    <img src=\"/assets/images/loyal.png\" class=\"features-ico\">\r\n                    <p>Loyalty Program</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"loyalprogram-solo-aling\">\r\n            <loyalty  class=\"loyalprogram\">\r\n        \r\n                <div class=\"loyalprogram-blocks-aling-2\">\r\n        \r\n                    <div class=\"\">\r\n                        <div class=\"loyalprogram-block-2\">\r\n                            <p>Level 1</p>\r\n                            <img src=\"/assets/images/loyalrang.gif\" class=\"loyalprogram-block-pic\">\r\n                            <h2>Life-Time 3% <br> Cashback In BC</h2>\r\n                        </div>\r\n                        <div class=\"loyalprogram-block-text\">\r\n                            <p>Spent 50 EUR <br> or 5 orders</p>\r\n                            <h2 class=\"active-h2\">100 Eur to bonuce</h2>\r\n                        </div>\r\n                    </div>\r\n        \r\n                    <div class=\"\">\r\n                        <div class=\"loyalprogram-block-2\">\r\n                            <p>Level 2</p>\r\n                            <img src=\"/assets/images/loyalrang.png\" class=\"loyalprogram-block-pic\">\r\n                            <h2>Life-Time 3% <br> Cashback In BC</h2>\r\n                        </div>\r\n                        <div class=\"loyalprogram-block-text\">\r\n                            <p>Spent 50 EUR <br> or 5 orders</p>\r\n                            <h2>100 Eur to bonuce</h2>\r\n                        </div>\r\n                    </div>\r\n        \r\n                    <div class=\"\">\r\n                        <div class=\"loyalprogram-block-2\">\r\n                            <p>Level 3</p>\r\n                            <img src=\"/assets/images/loyalrang.png\" class=\"loyalprogram-block-pic\">\r\n                            <h2>Life-Time 3% <br> Cashback In BC</h2>\r\n                        </div>\r\n                        <div class=\"loyalprogram-block-text\">\r\n                            <p>Spent 50 EUR <br> or 5 orders</p>\r\n                            <h2>100 Eur to bonuce</h2>\r\n                        </div>\r\n                    </div>\r\n        \r\n                    <div class=\"\">\r\n                        <div class=\"loyalprogram-block-2\">\r\n                            <p>Level 4</p>\r\n                            <img src=\"/assets/images/loyalrang.png\" class=\"loyalprogram-block-pic\">\r\n                            <h2>Life-Time 3% <br> Cashback In BC</h2>\r\n                        </div>\r\n                        <div class=\"loyalprogram-block-text\">\r\n                            <p>Spent 50 EUR <br> or 5 orders</p>\r\n                            <h2>100 Eur to bonuce</h2>\r\n                        </div>\r\n                    </div>\r\n        \r\n                    <div class=\"\">\r\n                        <div class=\"loyalprogram-block-2\">\r\n                            <p>Level 5</p>\r\n                            <img src=\"/assets/images/loyalrang.png\" class=\"loyalprogram-block-pic\">\r\n                            <h2>Life-Time 3% <br> Cashback In BC</h2>\r\n                        </div>\r\n                        <div class=\"loyalprogram-block-text\">\r\n                            <p>Spent 50 EUR <br> or 5 orders</p>\r\n                            <h2>100 Eur to bonuce</h2>\r\n                        </div>\r\n                    </div>\r\n        \r\n                </div>\r\n        \r\n            </loyalty>\r\n        </div>\r\n\r\n        <div class=\"loyalprogram-text\">\r\n            <p>\r\n                BuyBoosting\'s loyalty system is here and live, granting benefits to our most loyal buyer-base. \r\n                Right now, the reward program provides three levels of lifetime cashback percentages. H\r\n                ow it works is simple. The system operates automatically, and by paying money through your personal \r\n                BuyBoosting account, it automatically generates Boost Coins and unlocks loyalty tiers. \r\n                By purchasing multiple boosts or spending big on a single one can quickly open the tier 3 loyalty level, \r\n                which offers the highest Boost Coin cashback percentage. But there are more advantages to the Loyalty Program. \r\n                The BuyBoosting team has made it accessible for its clients to combine casual discount codes with Loyalty tier bonuses \r\n                to offer significant cost reductions and unbeatable prices in the boosting industry.\r\n            </p>\r\n        </div>\r\n\r\n    </gameslide>', '2021-04-20 10:30:59', '2021-04-20 12:34:33'),
(2, 'howitwork', 'How it work', 'How it work', 'How it work', NULL, '<gameslide class=\"gameslide\">\r\n\r\n        <div class=\"gameslide-aling\">\r\n            <div class=\"gameslide-block\">\r\n                <img src=\"./assets/images/game-bg/apexlarge.png\" class=\"game-large\">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"features-title-2\">\r\n            <div class=\"features-title-aling\">\r\n                <div class=\"features-title-slog\" style=\"margin-left: 587px!important;\">\r\n                    <img src=\"./assets/images/howit.gif\" class=\"features-ico\">\r\n                    <p>How it work</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"step-by-step s-b-s-active\">\r\n\r\n            <div class=\"step-by-step-flex\">\r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 1</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 2</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 3</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"step-by-step-flex\">\r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 4</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 5</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <img src=\"./assets/images/quest.png\" class=\"step-by-step-ico\">\r\n                        <p>Step 6</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n            </div>\r\n        \r\n\r\n        </div>\r\n\r\n\r\n        <div class=\"features-title-2 f-t-2-active\">\r\n            <div class=\"features-title-aling\">\r\n                <div class=\"features-title-slog\" style=\"margin-left: 587px!important;\">\r\n                    <img src=\"./assets/images/howit.png\" class=\"features-ico\">\r\n                    <p>Frequently Asked Questions</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"step-by-step s-b-s-active\">\r\n\r\n            <div class=\"step-by-step-flex\">\r\n\r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">Who would be playing on my account, or with me ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">How long will my order take to complete ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">I don\'t want to give access to my account, can we do this ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n\r\n            </div>\r\n\r\n            <div class=\"step-by-step-flex\" style=\"padding-bottom: 150px;\">\r\n\r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">Who would be playing on my account, or with me ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">How long will my order take to complete ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n    \r\n                <div class=\"step-by-step-blocks\">\r\n                    <div class=\"step-by-step-block\">\r\n                        <p style=\"font-weight: 600!important; padding-bottom: 12px;\">I don\'t want to give access to my account, can we do this ?</p>\r\n                    </div>\r\n                    <p>Go to check-out and add funds to purchase the \r\n                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>\r\n                </div>\r\n\r\n            </div>\r\n        \r\n\r\n        </div>\r\n\r\n    </gameslide>', '2021-04-20 12:35:17', '2021-04-20 12:35:17');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `game_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `game_id`, `name`, `avatar`, `text`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(2, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(3, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(4, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(5, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(6, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(7, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14'),
(8, 1, 4, 'Review', 'images/uploads/610388aa605636e1c2159b2315329aaa.jpg', 'Review', '2021-04-25 21:25:14', '2021-04-25 21:25:14');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` bigint UNSIGNED NOT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_address` longtext COLLATE utf8mb4_unicode_ci,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `contact_email`, `contact_address`, `copyright`, `created_at`, `updated_at`) VALUES
(1, '1@gmail.com', 'Test', 'All rights reserved. We care about your privacy. Copyright © 2021 boosting.gg, support@boosting.com', NULL, '2021-04-20 12:48:26');

-- --------------------------------------------------------

--
-- Структура таблицы `socials`
--

CREATE TABLE `socials` (
  `id` bigint UNSIGNED NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `socials`
--

INSERT INTO `socials` (`id`, `link`, `name`, `icon`, `created_at`, `updated_at`) VALUES
(1, '#', 'VK', 'images/uploads/d36e405c239258e2cd3103f4cbf2e4e1.png', '2021-04-25 20:45:17', '2021-04-25 20:46:45');

-- --------------------------------------------------------

--
-- Структура таблицы `tarifs`
--

CREATE TABLE `tarifs` (
  `id` bigint UNSIGNED NOT NULL,
  `game_id` bigint UNSIGNED DEFAULT NULL,
  `price` decimal(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `max_rating` bigint UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tarifs`
--

INSERT INTO `tarifs` (`id`, `game_id`, `price`, `name`, `description`, `created_at`, `updated_at`, `max_rating`) VALUES
(1, 4, '100.00', 'Test', 'description', '2021-04-26 20:47:17', '2021-04-26 22:41:36', 100),
(2, 1, '100.00', 'Test 2', 'description', '2021-04-26 20:47:17', '2021-04-26 22:41:42', 1000);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` tinyint DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Александр Шибистый', 'shibisty.a.wezom@gmail.com', '2021-04-18 23:49:13', '$2y$10$rCwvRnqLKFZ0hiH7GPUdluqwKgu4IianGQDIpwOkDuS9yBawvHiR6', NULL, '2021-04-12 09:13:56', '2021-04-12 09:13:56', 2),
(2, 'AS2', 'stud@gmail.com', '2021-04-11 23:54:38', '$2y$10$wWAV.AGRfIEi/LSeyMWbaeGn1EAkV7SjZBF3zljcKPrnWnha839ry', NULL, '2021-04-19 20:51:53', '2021-04-20 10:27:08', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blogs_slug_unique` (`slug`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `option_groups`
--
ALTER TABLE `option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders_options`
--
ALTER TABLE `orders_options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tarifs`
--
ALTER TABLE `tarifs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `metas`
--
ALTER TABLE `metas`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `option_groups`
--
ALTER TABLE `option_groups`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `orders_options`
--
ALTER TABLE `orders_options`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `socials`
--
ALTER TABLE `socials`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tarifs`
--
ALTER TABLE `tarifs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
