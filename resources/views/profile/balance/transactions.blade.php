@extends('layouts.profile')

@section('content')
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu-pro')
        </div>
        <div class="order-list-dyn-fix">
            @foreach($withdrawals as $key => $withdrawal)
            <div class="order-list-blocks">
                <div class="order-list-block">
                    <p>Id:<span style="margin-left: 3px; color: white;">{{($key + 1)}}</span></p>
                </div>
                <div class="order-list-block">
                    <p>
                        @if($withdrawal->status === \App\Models\Withdrawal::STATUS_SUCCESS)
                            You Received
                            <span style="margin-left: 3px; color: rgb(255, 86, 86);">{{$withdrawal->amount}}$</span>
                        @else
                            You earned
                            <span style="margin-left: 3px; color: rgb(92, 255, 86);">{{$withdrawal->amount}}$</span>
                        @endif
                    </p>
                </div>
                <div class="order-list-block">
                    <p>P.Date:<span style="margin-left: 3px; color: white;">{{$withdrawal->updated_at}}</span></p>
                </div>
                @if($withdrawal->status === \App\Models\Withdrawal::STATUS_CREATED)
                    <img src="/assets/images/details/load.gif" style="pointer-events: none;">
                    <div class="order-list-block">
                        <p>Please:<span style="margin-left: 3px; color: white;">always do job well</span></p>
                    </div>
                @else
                    @if($withdrawal->screenshot)
                        <div class="order-list-block">
                            <div class="screen-shot"></div>
                            <p><a href="{{url($withdrawal->screenshot)}}" target="_blank">Screenshot</a></p>
                        </div>
                    @endif
                    <div class="order-list-block">
                        <p>Pay:<span style="margin-left: 3px; color: white;">{{$withdrawal->payment_info}}</span></p>
                    </div>
                    <div class="order-list-block">
                        <p>Comment:<span style="margin-left: 3px; color: white;">{{$withdrawal->comment}}</span></p>
                    </div>
                @endif
            </div>
            @endforeach
        </div>
    </orderlist>
    <div style="padding-bottom: 250px;"></div>
    @include('components.footer')
@endsection
