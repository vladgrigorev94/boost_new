@extends('layouts.index')

@section('content')
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <div class="order-list-menu">
            @include('profile.particles.topmenu-pro-chat')
        </div>
    
    <?php $key = 0; ?>
    @include('profile.roles.components.chat')
    <div style="padding-bottom: 150px;"></div>
    @include('components.footer')
</div>
