
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu-user-inp')
        </div>
        @foreach($orders as $key => $order)
        <!-- <div class="order-list-margin"> -->
            @include('profile.roles.components.task-usr-inp')
        <!-- </div> -->
        @endforeach
    </orderlist>
    @include('profile.particles.loyalty')
    
    @include('components.footer')

</div> 

