@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
@endif
    <div class="order-list-blocks-2">

        <div class="order-list-block">
            <p>Id:<span style="margin-left: 3px; color: white;">{{$key + 1}}</span></p>
        </div>

        <div class="order-list-block">
            <p>Game:<span style="margin-left: 3px; color: white;">{{$order->tarif->game->name}}</span></p>
        </div>

        <div class="order-list-block">
            <p>Service:<span style="margin-left: 3px; color: white;">{{$order->tarif->name}}</span></p>
        </div>

        <div class="order-list-block">
            <p>Platform:<span style="margin-left: 3px; color: white;">{{$order->tarif->platform->name}}</span></p>
        </div>
        <div class="order-list-block">
            <label for="hider2" class="clickme-button" id="clickme-{{$order->id}}">
                    <p>All Options</p>
            </label>
            <input type="checkbox" id="hider2">
            <div class="content2">
                <div class="additional-list-block">
                    @if($order->options)
                        @foreach (unserialize($order->options) as $option)
                        <div class="additional-list"> 
                            <p>{{$option['name']}}</p>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="order-list-block">
            <p>Price:<span style="margin-left: 3px; color: white;">${{$order->amount}}</span></p>
        </div>

        <div class="order-list-block">
            <p>Status:<span style="margin-left: 3px; color: white;">@include('profile.particles.status')</span></p>
        </div>

        <div class="order-list-block">
            <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
        </div>


        <div class="end-dead">
                <img src="/assets/images/dead.gif" class="end-ico">
            </div>


    </div>
    
@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
@endif
