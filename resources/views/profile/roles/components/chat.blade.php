<div class="order-list-blocks-detail">
        <div class="order-list-block">
        <p>Id:<span style="margin-left: 3px; color: white;">{{$key + 1}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Game:<span style="margin-left: 3px; color: white;">{{$order->tarif->game->name}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Service:<span style="margin-left: 3px; color: white;">{{$order->tarif->name}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Platform:<span style="margin-left: 3px; color: white;">{{$order->tarif->platform->name}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Price:<span style="margin-left: 3px; color: white;">${{$order->amount}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Status:<span style="margin-left: 3px; color: white;">@include('profile.particles.status')</span></p>
    </div>
    <div class="order-list-block">
        <p>Progress:<span style="margin-left: 3px; color: white;">{{$progress !== null ? $progress : '0%'}}</span></p>
    </div>
    <form style="position: absolute;" action="{{route('order.broke')}}" method="POST">
        @csrf
        <input type="hidden" value="{{$order->id}}" name="order_id">
        <input type="hidden" value="3" name="status">
        <button type="submit" id="broke-btn">Broke Task</button>
    </form>
</div>



<div class="show-details">

    <div class="account-info">
    <p>Login: <b>{{$order->account_login}}</b></p>
    <p> Password: <b>{{$order->account_password}}</b></p>
    <p>Country: <b>{{$order->account_country}}</b></p>
    </div>

    <div class="show-details-aling">
        <div class="chat">
            <div id="chat-bg">
                
                <ul id="messages"></ul>
                <div class="messages-content">
                    @foreach($messages as $message)
                        @if(\Arr::get($message, 'to_id') === $user->id)
                            <div class="chat-msg-toyou"><p>{{\Arr::get($message, 'message')}}</p></div>
                        @else
                            <div class="chat-msg-you"><p>{{\Arr::get($message, 'message')}}</p></div>
                        @endif
                    @endforeach
                </div>
            </div>
            <form style="position: relative;" id="form" method="POST" action="{{route('message.add')}}">
                <div class="input-chat">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input id="message" name="message" placeholder="Write Message" autocomplete="off">
                    <button type="submit" id="send-chat">Send</button>
                </div>
            </form>
            <div class="dit-progress-bar">
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                    <p>Starting <br> <span>{{$order->min_rating}}</span></p>
                </div>
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.gif" class="dit-progress-ico">
                    <p class="active-dit-pg-bar">Currently <br> <span class="active-dit-pg-span">{{$progress ? $progress : $order->min_rating}}</span></p>
                </div>
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                    <p>Purpose <br> <span>{{$order->max_rating}}</span></p>
                </div>
                <div class="order-btn-aling">
                    @if(\Auth::user()->id === $order->prof_id)
                    <form action="{{route('order.end')}}" method="POST">
                        @csrf
                        <input type="hidden" value="{{$order->id}}" name="order_id">
                        <input type="hidden" value="2" name="status">
                        <button type="submit" id="end-work">End Work</button>
                    </form>
                    <div class="input-details">
                        <form style="position: relative;" action="{{route('progress.handler')}}" method="POST">
                            @csrf
                            <input id="message" type="text" name="status" placeholder="Set Progress" autocomplete="off">
                            <input type="hidden" value="{{$order->id}}" name="order_id">
                            <button type="submit" id="save-progress">Save</button>
                        </form>
                    </div>
                    @endif
                    @if(\Auth::user()->id === $order->client_id)

                    @endif
                </div>
            </div>
        </div>
        @if(\Auth::user()->id === $order->client_id)
        <div class="dit-avatar">
            <div class="dit-avatar-prof">
                <img class="profile-ava-2" src="{{$order->prof->avatar ? url($order->prof->avatar) : '/assets/images/avatars/2.gif'}}">
            </div>
            <div class="dit-avatar-name">
                <h3>{{$order->prof->name}}</h3>
                <p>Worked</p>
            </div>
            <div class="what-game">
                <p>{{$order->tarif->game->name}} <br> <b style="text-transform: uppercase;">{{$order->tarif->platform->name}}</b></p>
            </div>
        </div>
        @else
        <div class="dit-avatar">
            <div class="dit-avatar-prof">
                <img class="profile-ava-2" src="{{$order->client->avatar ? url($order->client->avatar) : '/assets/images/avatars/2.gif'}}">
            </div>
            <div class="dit-avatar-name">
                <h3>{{$order->client->name}}</h3>
                <p>Client</p>
            </div>
            <div class="what-game">
                <p>{{$order->tarif->game->name}} <br> <b style="text-transform: uppercase;">{{$order->tarif->platform->name}}</b></p>
            </div>
        </div>
        @endif
    </div>
</div>