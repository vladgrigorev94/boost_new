
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu-usr-end')
        </div>
        <div class="order-list-dyn-fix">
        @foreach($orders as $key => $order)
            @include('profile.roles.components.task-usr-end')
        @endforeach
        </div>
    </orderlist>
    @include('components.footer')
</div> 
