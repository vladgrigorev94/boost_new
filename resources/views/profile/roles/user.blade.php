
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu-user')
        </div>
        <div class="order-list-margin">
            @foreach($orders as $key => $order)
                @include('profile.roles.components.task-usr-home')
            @endforeach
        </div>
    </orderlist>
    @include('profile.particles.loyalty')
    @include('components.footer')
</div>
