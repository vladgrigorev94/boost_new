@extends('layouts.profile')

@section('content')
    @if($user->role === \App\Models\User::ROLE_USER)
        @include('profile.roles.components.user-inpgrs')
    @else
        @include('profile.roles.components.prof-inpgrs')
    @endif
@endsection
