<?php

$orders = \App\Models\Order::where('prof_id', \Auth::user()->id)
    ->where('status', \App\Models\Order::STATUS_SUCCESS)
    ->get();

$balanceErned = 0.00;
$balanceAvailable = \Auth::user()->balance;
$retention = 0.00;

foreach($orders as $order) {
    $balanceErned += (double)$order->amount;

    if((strtotime($order->updated_at)) > (time() - \App\Models\User::RETENTION_TIME)) {
        $balanceAvailable -= $order->amount;
    }
}

?>

<div class="balance-menu-block">
    <h2>Balance:</h2>
    <div class="blance-available-aling">
        <div class="balance-available">
            <p>Available: <span style="color: white;">{{$balanceAvailable}}$</span></p>
        </div>
        <div class="balance-available-2">
            <p>Total: <span style="color: white;">{{\Auth::user()->balance}}$</span></p>
        </div>
        <div class="balance-available-3">
            <p>Earned: <span style="color: white;">{{$balanceErned}}$</span></p>
        </div>
    </div>
</div>
