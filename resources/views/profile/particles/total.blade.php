@if(\Auth::user()->role === \App\Models\User::ROLE_USER)
    <div class="profile-menu">
        <div class="profile-avatar-aling">
            <div class="profile-avatar">
                <img class="profile-ava" src="/assets/images/balance.png">
            </div>
        </div>
        @include('profile.particles.balance-user')
        <div class="out-edit">
            <div class="out">
                <a href="{{route('logout')}}">Log out</a>
                <img src="/assets/images/log.png" class="out-icon">
            </div>
            <label class="edit-prof" for="hider3" id="clickme3">
                    <p>Edit Profile</p>
                    <img src="/assets/images/edit.png" class="edit-prof-icon">
            </label>
            <input type="checkbox" id="hider3">
            @include('profile.particles.editProfile')
        </div>
    </div>
@else
    <div class="profile-menu">
        <div class="profile-avatar-aling">
            <div class="profile-avatar">
                <img class="profile-ava" src="/assets/images/balance.png">
            </div>
        </div>
        @include('profile.particles.balance-prof')
        <div class="out-edit">
            <div class="out">
                <a href="{{route('logout')}}">Log out</a>
                <img src="/assets/images/log.png" class="out-icon">
            </div>
            <label class="edit-prof" for="hider3" id="clickme3">
                    <p>Edit Profile</p>
                    <img src="/assets/images/edit.png" class="edit-prof-icon">
            </label>
            <input type="checkbox" id="hider3">
            @include('profile.particles.editProfile')
        </div>
    </div>
@endif
