@if($order->status === \App\Models\Order::STATUS_CREATED)
Created
@elseif($order->status === \App\Models\Order::STATUS_INPROGRESS)
In progress
@elseif($order->status === \App\Models\Order::STATUS_SUCCESS)
Success
@elseif($order->status === \App\Models\Order::STATUS_FAILURE)
Failure
@endif
