<div class="content3">
    <div class="edit-modal-block">
        <form action="{{route('profile.edit')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="log-modal-file"> 
                <p>Name</p>
                <input type="text" id="" value="" name="name" placeholder="{{\Auth::user()->name ? \Auth::user()->name : 'Set new Name'}}" autocomplete="off">
            </div>
            <div class="log-modal-file">
                <p>Email</p>
                <input type="email" id="" value="" name="email" placeholder="{{\Auth::user()->email ? \Auth::user()->email : 'Set new Email'}}" autocomplete="off">
            </div>
            <div class="log-modal-file">
                <p>Password</p>
                <input type="password" id="" value="" name="password" placeholder="Set new Password" autocomplete="off">
            </div>
            <div class="log-modal-file">
                <p>Avatar</p>
                <input accept=".jpg, .png, .gif" name="file" type="file" id="file" style="opacity: 0; position: absolute;">
                <label for="upload-file__input_1">
                    <div class="www"><p>Click here to upload Avatar</p></div>
                </label>
            </div>
            <button class="reg-modal-log-login">Change</button>
        </form>
    </div>
</div>
