<div class="order-list-menu-aling">
    <div class="order-pad">
        <img src="/assets/images/active.gif" class="order-icon">
        <a href="{{route('home')}}">All orders</a>
    </div>
    <div class="order-pad-2">
        <img src="/assets/images/progress.png" class="order-icon">
        <a href="{{route('inprogress')}}">In progress</a>
    </div>
    <div class="order-pad-2">
        <img src="/assets/images/dragonico.png" class="order-icon">
        <a href="{{route('endorders')}}">End orders</a>
    </div>
    <div class="order-pad-2">
        <img src="/assets/images/dragonico.png" class="order-icon">
        <a href="{{route('transactions')}}">Transactions</a>
    </div>
    <div class="order-pad-2">
        <img src="/assets/images/dragonico.png" class="order-icon">
        <a href="{{route('paymentrequests')}}">Payment requests</a>
    </div>       
</div>
