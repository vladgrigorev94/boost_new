<downmenu class="down-menu">
    <div class="down-menu-aling">
        <div class="down-pad">
            <img src="/assets/images/icons/cont.png" class="down-menu-icon">
            <a href="{{route('contacts')}}">Contacts</a>
        </div>
        <div class="down-pad">
            <img src="/assets/images/icons/loy.png" class="down-menu-icon">
            <a href="{{route('page', ['slug' => 'loyalityprogram'])}}">Loyalty Program</a>
        </div>
        <div class="down-pad">
            <img src="/assets/images/icons/blog.png" class="down-menu-icon">
            <a href="{{route('blog')}}">Blog</a>
        </div>
        <div class="down-pad" style="border-right: none!important;">
            <img src="/assets/images/icons/how.png" class="down-menu-icon">
            <a href="{{route('page', ['slug' => 'howitwork'])}}">How it work ?</a>
        </div>
    </div>
</downmenu>