
<header class="top-menu">

<a href="{{url('/')}}">
    <div class="menu-pad">
        <a href="{{url('/')}}">Boostmakers</a>
        <a href="{{url('/')}}">
        <img src="/assets/images/logo-an.gif" class="menu-icon">
        </a>
    </div>
</a>

    @foreach(config('games') as $game)
<a href="{{route('game', ['name' => $game->slug])}}">
    <div class="menu-pad-g">
        <img src="/assets/images/pulse.png" class="menu-icon-p">
        <a href="{{route('game', ['name' => $game->slug])}}">{{$game->name}}</a>
        @if($game->thumb)
        <a href="{{route('game', ['name' => $game->slug])}}">
        <img src="{{url($game->thumb)}}" class="menu-icon-g">
        </a>
        @endif
    </div>
</a>
    @endforeach

    <div class="reg-log">

        <div class="log">
            @if(!\Auth::check())

            <input type="checkbox" id="hider">
            <div class="content">
                <div class="pay-request-block-4">
                    <form action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="log-modal-file"> 
                            <p>Name</p>
                            <input type="email" id="" value="{{old('email')}}" name="email" placeholder="Email" autocomplete="off">
                        </div>
                        <div class="log-modal-file">
                            <p>Password</p>
                            <input type="password" id="" name="password" placeholder="Password" autocomplete="off">
                        </div>
                        <button class="log-modal-log-login">Login</button>
                    </form>
                </div>
            </div>
            @endif
            <a href="{{route('logout')}}">
            <label class="edit-prof-2" for="hider" id="clickme">
                    <p>{{!Auth::check() ? 'Login' : 'Log out'}}</p>
                    <img src="/assets/images/log.gif" class="edit-prof-icon">
            </label>
            </a>
    </div>

        <div class="reg">
            @if(!\Auth::check())
<!--
                <form action="{{route('register')}}" method="POST">
                @csrf
                <div class="reg-modal">           
                    <div class="reg-modal-log">
                        <p>Email</p>
                        <input type="email" id="" value="{{old('email')}}" name="email" placeholder="Email" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Name</p>
                        <input type="text" id="" name="name" value="{{old('name')}}" placeholder="Name" autocomplete="off">
                    </div>  
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password" placeholder="Password" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password_confirmation" placeholder="Password confirm" autocomplete="off">
                        <input type="hidden" name="role" value="0">
                    </div>
                    
                    <button class="log-modal-log-login">
                        Registration
                    </button>
                </div>
            </form>
-->
            <a href="{{route('join')}}">Registration</a>
            @else
            <a href="{{route('home')}}">Your Profile</a>
            @endif

            <img src="/assets/images/edit.gif" class="reg-icon">
        </div>

    </div>


</header>
