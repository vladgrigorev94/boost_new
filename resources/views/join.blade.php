
<link rel="stylesheet" href="assets/css/style2.css">

<div class="boxed_wrapper">

    <reg-home>
    @if(!\Auth::check())
    <form action="{{route('register')}}" method="POST">
            @csrf
            <div class="reg-mod-aling">
                <div class="join-aling">
                    <p>Join to us!</p>
                </div>
                <div class="reg-mod">           
                    <div class="reg-modal-log">
                        <p>Email</p>
                        <input type="email" id="" value="{{old('email')}}" name="email" placeholder="Email" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Name</p>
                        <input type="text" id="" name="name" value="{{old('name')}}" placeholder="Name" autocomplete="off">
                    </div>  
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password" placeholder="Password" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password_confirmation" placeholder="Password confirm" autocomplete="off">
                        <input type="hidden" name="role" value="0">
                    </div>
                    
                    <button class="reg-modal-log-login">Registration</button>
                </div>
                <img src="./assets/images/logocom.gif" class="logo-com2">
                <div class="b-home">
                    <a href="{{url('/')}}">Back to Home</a>
                </div>
            </div>
    </form>
    @else
            <?php 
            Route::get('home', function () {
            return redirect('home');
            }); 
            ?>
    @endif

    </reg-home>
