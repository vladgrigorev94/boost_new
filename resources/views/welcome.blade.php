@extends('layouts.index')

@section('content')

<div class="boxed_wrapper">
    @include('components.header')
    <downmenu class="down-menu">

        <div class="down-menu-aling">
            <div class="down-pad">
                <img src="/assets/images/icons/cont.png" class="down-menu-icon">
                <a href="{{route('contacts')}}">Contacts</a>
            </div>
    
            <div class="down-pad">
                <img src="/assets/images/icons/loy.png" class="down-menu-icon">
                <a href="{{route('page', ['slug' => 'loyalityprogram'])}}">Loyalty Program</a>
            </div>
    
            <div class="down-pad">
                <img src="/assets/images/icons/blog.png" class="down-menu-icon">
                <a href="{{route('blog')}}">Blog</a>
            </div>
    
            <div class="down-pad" style="border-right: none!important;">
                <img src="/assets/images/icons/how.png" class="down-menu-icon">
                <a href="{{route('page', ['slug' => 'howitwork'])}}">How it work ?</a>
            </div>
        </div>

    </downmenu>

    <div class="home-img-bg">
        <img src="/assets/images/details/bgh.png" class="">
    </div>

    <serviceblock class="service-block">

        <div class="service-block-aling">

            <div>
                <div class="block-s">

                    <p>Apex Legend</p>
        
                    <div>
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Win Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 70px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Kill Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 75px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Power Leveling</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 60px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Achievements</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                    </div>

                    <img src="/assets/images/game-bg/apex1.gif" class="game-bg">
        
                </div>
            </div>
    
            <div>
                <div class="block-s">

                    <p>Valorant</p>
        
                    <div>
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Win Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 70px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Kill Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 75px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Power Leveling</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 60px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Achievements</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                    </div>

                    <img src="/assets/images/game-bg/apex1.gif" class="game-bg">
        
                </div>
            </div>
    
            <div>
                <div class="block-s">

                    <p>Dota 2</p>
        
                    <div>
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Win Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 70px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Kill Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 75px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Power Leveling</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 60px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Achievements</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                    </div>

                    <img src="/assets/images/game-bg/dt2.gif" class="game-bg">
        
                </div>
            </div>
    
            <div>
                <div class="block-s">

                    <p>Call of Duty: Warzone</p>
        
                    <div>
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Win Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 70px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Kill Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 75px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Power Leveling</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 60px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Achievements</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                    </div>

                    <img src="/assets/images/game-bg/apex1.gif" class="game-bg">
        
                </div>
            </div>
    
            <div>
                <div class="block-s">

                    <p>Call of Duty: BO Cold War</p>
        
                    <div>
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Win Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 70px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Rank Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
            
                        <div class="service-1">
                            <a href="">Kill Boosting</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 75px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Power Leveling</a>
                            <img src="/assets/images/arrow.png" class="service-icon" style="padding-left: 60px!important;">
                        </div>
            
                        <div class="service-1">
                            <a href="">Achievements</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                    </div>

                    <img src="/assets/images/game-bg/apex1.gif" class="game-bg">
        
                </div>
            </div>

        </div>
        
    </serviceblock>

    <features class="features">

        <div>
            <img src="" class="">

            <div class="features-title">
                <div class="features-title-aling">
                    <div class="features-title-slog">
                        <img src="/assets/images/features.png" class="features-ico">
                        <p>Our Features</p>
                    </div>
                </div>
            </div>
    
            <div class="features-blocks">
    
                <div class="features-pack-block">
                    <img src="/assets/images/block-ico.png" class="features-blocks-ico">
    
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>VPN Protection</h1>
                        <p>
                            BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                            The measure helps us to operate in maximum safety. 
                            Our platform recognizes the country of the buyer, 
                            and the boosters use the information to set their VPNs to the same 
                            location before accessing an account.
                        </p>
                    </div>
                </div>
    
                <div class="features-pack-block">
    
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>Order Tracking</h1>
                        <p>
                            Our platform provides order tracking and scheduling for all users. 
                            When you submit a boosting order, it creates a private section for you in the member's area. 
                            In your order, you can follow up on the progress made by the booster and also schedule boosting hours in the chat.
                            In your order, you can follow
                        </p>
                    </div>
    
                    <img src="/assets/images/block-ico-or.png" class="features-blocks-ico-2">
                </div>
    
            </div>
    
            <div class="features-blocks">
    
                <div class="features-pack-block">
                    <img src="/assets/images/block-ico-p1.png" class="features-blocks-ico">
    
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>VPN Protection</h1>
                        <p>
                            BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                            The measure helps us to operate in maximum safety. 
                            Our platform recognizes the country of the buyer, 
                            and the boosters use the information to set their VPNs to the same 
                            location before accessing an account.
                        </p>
                    </div>
                </div>
    
                <div class="features-pack-block">
    
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>Order Tracking</h1>
                        <p>
                            Our platform provides order tracking and scheduling for all users. 
                            When you submit a boosting order, it creates a private section for you in the member's area. 
                            In your order, you can follow up on the progress made by the booster and also schedule boosting hours in the chat.
                            In your order, you can follow
                        </p>
                    </div>
    
                    <img src="/assets/images/block-ico-p2.png" class="features-blocks-ico-2">
                </div>
    
            </div>
        </div>

    </features>

    <commblock class="comm-blocks">

        <div class="comm-blocks-aling">

            <div class="comm-blocks-center">

                <img src="/assets/images/logocom.gif" class="logo-com">

                <div class="comm" style="margin-right: 620px;">
                    <h2 style="text-align: left!important;">Review from users</h2>
    
                    <div class="comm-block">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                    <div class="comm-block" style="margin-left: 30px;">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                    <div class="comm-block" style="margin-left: 120px;">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                </div>
    
                <div class="comm" style="margin-left: 740px;">
                    <h2>Review from users</h2>
    
                    <div class="comm-block">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                    <div class="comm-block" style="margin-left: -30px;">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                    <div class="comm-block" style="margin-left: -120px;">
                        <img src="/assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Joshua</h3>
                                <p>Verified</p>
                            </div>
                            <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                        </div>
                    </div>
    
                </div>

            </div>

            <div class="">
                <input type="button" value="Load More" class="more-comm">
            </div>

        </div>

    </commblock>

    <boosting class="boosting-info">

        <div class="features-title">
            <div class="features-title-aling">
                <div class="features-title-slog">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>Boosting Platform</p>
                </div>
            </div>
        </div>
        <div class="boosting-blocks">

            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>VPN Protection</h1>
                    <p>
                        BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                        The measure helps us to operate in maximum safety. 
                        Our platform recognizes the country of the buyer, 
                        and the boosters use the information to set their VPNs to the same 
                    </p>
                </div>
            </div>

            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>VPN Protection</h1>
                    <p>
                        BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                        The measure helps us to operate in maximum safety. 
                        Our platform recognizes the country of the buyer, 
                        and the boosters use the information to set their VPNs to the same 
                    </p>
                </div>
            </div>

            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>VPN Protection</h1>
                    <p>
                        BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                        The measure helps us to operate in maximum safety. 
                        Our platform recognizes the country of the buyer, 
                        and the boosters use the information to set their VPNs to the same 
                    </p>
                </div>
            </div>

        </div>

        <div class="boosting-blocks" style="justify-content: center;">

            <div class="boosting-pack-block" style="margin-right: 150px;">
                <img src="/assets/images/block-ico-p1.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>VPN Protection</h1>
                    <p>
                        BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                        The measure helps us to operate in maximum safety. 
                        Our platform recognizes the country of the buyer, 
                        and the boosters use the information to set their VPNs to the same 
                    </p>
                </div>
            </div>

            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-p1.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>VPN Protection</h1>
                    <p>
                        BuyBoosting's boosters utilize VPN services before connecting to the user's account. 
                        The measure helps us to operate in maximum safety. 
                        Our platform recognizes the country of the buyer, 
                        and the boosters use the information to set their VPNs to the same 
                    </p>
                </div>
            </div>

        </div>

    </boosting>

    <moreinfo-2>

        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/bullet.gif" class="features-ico" style="height: 30px!important;">
                    <p>More information</p>
                </div>
            </div>
        </div>
    
        <div class="step-by-step s-b-s-active-2" style="padding-bottom: 150px;">
    
            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/bullet.gif" class="step-by-step-ico">
                        <p>Job</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                       <br>years have very strict instructions to look out for our</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/bullet.gif" class="step-by-step-ico">
                        <p>Location</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                        <br>years have very strict instructions to look out for our</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/bullet.gif" class="step-by-step-ico">
                        <p>Customer service</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                        <br>years have very strict instructions to look out for our</p>
                </div>
            </div>
    
            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Power</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Create</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Boost</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
            </div>

            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/loyal.png" class="step-by-step-ico">
                        <p>Job</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                       <br>years have very strict instructions to look out for our</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/loyal.png" class="step-by-step-ico">
                        <p>Location</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                        <br>years have very strict instructions to look out for our</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/loyal.png" class="step-by-step-ico">
                        <p>Customer service</p>
                    </div>
                    <p>Most definitely. Our boosters have been working with,
                        <br>years have very strict instructions to look out for our</p>
                </div>
            </div>
        
        </div>

    </moreinfo-2>
    @include('components.footer')
</div>
</body>
