@extends('layouts.profile')
@section('content')
    @if($user->role === \App\Models\User::ROLE_PROF)
        @include('profile.user')
    @elseif($user->role === \App\Models\User::ROLE_ADMIN)
        @include('profile.user')
    @else
        @include('profile.prof')
    @endif
@endsection
