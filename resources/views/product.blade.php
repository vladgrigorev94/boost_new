@extends('layouts.index')
<link href="/assets/js/jquery-ui.css" rel="stylesheet">

@section('content')
<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        @if($game->image)
        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="{{url($game->image)}}" class="game-large">
            </div>
        </div>
        @endif
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>Create Order</p>
                </div>
            </div>
        </div>
        <div class="create-order-bg">
            <div class="create-order-full">
                <div class="create-order-title">
                    <img src="/assets/images/minigun.png" class="">
                    <p>{{$game->name}}</p>
                    <h1>{{$tarif->name}}</h1>
                </div>
                @if(\Auth::check())
                <form action="{{route('order')}}" method="POST" id="order-form">
                    @csrf
                @endif
                    <div>
                        <div class="calt-aling">
                            <div class="filter__cost table">
                            <div class="table-title">
                                <div class="table-text-1"><p>Start Rrank</p></div>
                                <img src="/assets/images/shield.png" class="shield-ico">
                                <div class="table-text-2"><p>End Rrank</p></div>
                            </div>
                                <div class="table-aling">
                                    <div class="table-cell">
                                        <input id="priceMin" name="min_rating" type="text" value="0" class="form-control">
                                    </div>
                                    <div class="table-cell">
                                        <input id="priceMax" name="max_rating" type="text" value="{{$tarif->max_rating}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="filter__block filter__block--slider">
                                <div class="filter__slider">
                                    <div id="filter__range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                                        <span class="ui-slider-handle ui-corner-all ui-state-default" style="left: 35%!important;"></span>
                                        <span  class="ui-slider-handle ui-corner-all ui-state-default" style="margin-left: 0px;"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ac-info">
                        <div class="log-modal-file2"> 
                            <p>Account login</p>
                            <input type="text" id="" value="" name="account_login" placeholder="account login" autocomplete="off">
                        </div>
                    </div>

                    <div class="ac-info">
                        <div class="log-modal-file2"> 
                            <p>Account password</p>
                            <input type="text" id="" value="" name="account_password" placeholder="account password" autocomplete="off">
                        </div>
                    </div>

                    <div class="ac-info">
                        <div class="log-modal-file2"> 
                            <p>Your country</p>
                            <input type="text" id="" value="" name="account_country" placeholder="your country (for VPN)" autocomplete="off">
                        </div>
                    </div>

                    <div class="order-check-box">
                        @foreach($tarif->groups as $group)
                        <div class="order-check-box-aling">
                            <p>{{$group->name}}</p>
                            <div class="checkbox-aling">
                                @foreach($group->options as $option)
                                <label class="container">{{$option->name}}
                                    <input type="checkbox" 
                                        checked="checked"
                                        data-amount="{{$option->amount}}"
                                        data-bonus="{{$option->bonus_cost}}"
                                        name="OPTIONS[{{$option->id}}]" 
                                        value="1"
                                    >
                                    <span class="checkmark"></span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                @if(\Auth::check())
                    <div class="create-order-pads">
                        <input type="hidden" value="{{$tarif->id}}" name="tarif_id">
                        <input type="hidden" value="{{$tarif->price}}" name="amount">

                        <button type="submit" id="buy-ord-btn">Buy Service</button>
                        <div class="how-much"><p><span id="price" data-original="{{$tarif->price}}">{{$tarif->price}}</span>$</p></div>
                        <div class="how-much-2"><p>Bonuce: <span style="color: #ffffff;"><span id="bonus" data-original="{{$tarif->bonus_cost}}">{{$tarif->bonus}}</span> coins</span></p></div>
                        <div class="how-much-5"><a href="#" id="buy-ord-btn2"><span style="color: #ffae5b;">Bonuce:</span> {{$tarif->bonus_cost}} coins</a></div>
                    </div>
                @else
                    <div class="create-order-pads">
                        <input type="hidden" value="{{$tarif->id}}" name="tarif_id">
                        <input type="hidden" value="{{$tarif->price}}" name="amount">
                        <a href="{{route('join')}}"><button id="buy-reg-btn">Registration</button></a>
                        <div class="how-reg"><p>You must register to purchase</p></div>
                        <div class="how-much"><p><span id="price" data-original="{{$tarif->price}}">{{$tarif->price}}</span>$</p></div>
                    </div>
                </form>
                @endif
            </div>
        </div>
<!--
            @if(count($reviews))
        <div class="review-details">
            <div class="review-details">
                <h2 style="text-align: left!important;">Review from users</h2>
                @foreach($reviews as $chunk)
                <div class="review-details-aling">
                    @foreach($chunk as $review)
                    <div class="comm-block-2">
                        <img src="{{isset($review->avatar) ? '/'.$review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <a href="{{route('reviews')}}" class="more-comm2">Load more</a>
            </div>
        </div>
        @endif
-->
<div class="review-details">

    <h2 style="text-align: left!important;">Review from users</h2>

    <div class="review-details-aling">

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>

    </div>

    <div class="review-details-aling">

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>

        <div class="comm-block-2">
            <img src="/assets/images/coom-ico.png" class="comm-ico">
            <div class="comm-text-block">
                <div class="comm-name">
                    <h3>Joshua</h3>
                    <p>Verified</p>
                </div>
                <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
            </div>
        </div>
    
</div>

<input type="button" value="Load More" class="more-comm2">

</div>
        <div class="step-by-step">
            <div class="step-by-step-flex" >
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_20.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_20.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_21.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_21.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_22.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_22.content')}}</p>
                </div>
            </div>
            <div class="step-by-step-flex"  style="padding-bottom: 150px;">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_23.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_23.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_24.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_24.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_25.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_25.content')}}</p>
                </div>
            </div>
        </div>
    </gameslide>
    @include('components.footer')
</div>

    <script src="/assets/js/external/jquery/jquery.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>
    <script src="/assets/js/polzunok.js"></script>

<script>
$(function() {
    var comparePrice = function() {
        var originalPrice = Number($('#price').data('original'));

        $('[name^="OPTIONS"]:checked').each(function(index, el) {
            var jEl = $(el);
            var price = Number(jEl.data('amount'));
            originalPrice += price;
        });

        $('#price').text(originalPrice);
    }

    var compareBonus = function() {
        var originalBonus = Number($('#bonus').data('original'));

        $('[name^="OPTIONS"]:checked').each(function(index, el) {
            var jEl = $(el);
            var bonus = Number(jEl.data('bonus'));
            originalBonus += bonus;
        });

        $('#bonus').text(originalBonus);
    }

    compareBonus();

    $(document).on('change', '[name^="OPTIONS"]', function(event) {
        comparePrice();
        compareBonus();
    });

    $(document).on('click', '#buy-ord-btn2', function(event) {
        event.preventDefault();
        var buyForBonusUrl = '{{route('order.bonus')}}';
        var orderForm = $('#order-form');

        orderForm.attr('action', buyForBonusUrl);
        orderForm.submit();
    });

    var priceValue = '{{$tarif->price}}';
    var priceForRating = '{{$tarif->price_for_rating}}';
    var changePrice = function() {
        var MAX_VALUE_SELECTOR = '#priceMax';
        var MIN_VALUE_SELECTOR = '#priceMin';

        var maxValue = Number($(MAX_VALUE_SELECTOR).val());
        var minValue = Number($(MIN_VALUE_SELECTOR).val());
        var value = maxValue - minValue;

        var price = Number($('#price').text());

        var newPrice = value > 0 && value !== 'NaN' ? price + (value * priceForRating) : price;
        console.log(newPrice);
        $('#price').text(newPrice);
        $('#price').data('original', newPrice);
    }

    var oldMaxRating = Number($('#priceMax').val());
    var oldMinRating = Number($('#priceMin').val());
    
    setInterval(function() {
        var maxRating = Number($('#priceMax').val());
        var minRating = Number($('#priceMin').val());

        if((oldMaxRating != maxRating) || (oldMinRating != minRating)) {
            $('#price').text(priceValue);
            $('#price').data('original', priceValue);
            
            oldMaxRating = maxRating;
            oldMinRating = minRating;
            
            changePrice();
            comparePrice();
        }
    }, 100);

    changePrice();
    comparePrice();
});
</script>
@endsection
