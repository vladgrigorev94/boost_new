@extends('layouts.index')

@section('content')

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        @if($game->image)
        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="{{$game->image}}" class="game-large">
            </div>
        </div>
        @endif
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>{{$game->name}}</p>
                </div>
            </div>
        </div>
        @foreach($tarifs as $tarifChunk)
        <div class="game-service-block-aling">
            <img src="" class="">
            @foreach($tarifChunk as $tarif)
            <div class="game-service-block"
                @if($tarif->background)
                    style="background: url({{url($tarif->background)}});"
                @endif
            >
                <img src="/assets/images/arrow.png" class="game-service-img">
                <div class="game-service-txt-1">
                    <img src="">
                    <p>{{$tarif->platform->name}}</p>
                    <h1>{{$tarif->name}}</h1>
                </div>
                <div class="game-service-txt-2">
                    <p>{{$tarif->description}}</p>
                </div>
                <div class="">
                    <div class="create-order-pads-2">
                        <button onclick="window.location.href='{{route('product', ['id' => $tarif->id])}}';" type="submit" id="buy-ord-btn-2">Buy Service</button>
                        <div class="how-much-2"><p>{{$tarif->price}}$</p></div>
                        <div class="how-much-5"><a href="" id="buy-ord-btn2"><span style="color: #ffae5b;">Bonuce:</span> {{$tarif->bonus_cost}} coins</a></div>
                    </div>
                </div>
            </div>
            @endforeach 
        </div>
        @endforeach
<!--
        @if(count($reviews))
        <div class="review-details-al">
            <div class="review-details">
                <h2 style="text-align: left!important;">Review from users</h2>
                @foreach($reviews as $chunk)
                <div class="review-details-aling">
                    @foreach($chunk as $review)
                    <div class="comm-block-2">
                        <img src="{{isset($review->avatar) ? '/'.$review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <a href="{{route('reviews')}}" class="more-comm2">Load more</a>
            </div>
        </div>
        @endif
-->

<div class="review-details">

            <h2 style="text-align: left!important;">Review from users</h2>

            <div class="review-details-aling">

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>

            </div>

            <div class="review-details-aling">

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>

                <div class="comm-block-2">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>Joshua</h3>
                            <p>Verified</p>
                        </div>
                        <p>Really professional website and boosting service. The service has loads of professional boosters.</p>
                    </div>
                </div>
                
            </div>

            <input type="button" value="Load More" class="more-comm2">

        </div>

        <div class="step-by-step">
            <div class="step-by-step-flex" >
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_20.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_20.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_21.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_21.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_22.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_22.content')}}</p>
                </div>
            </div>
            <div class="step-by-step-flex"  style="padding-bottom: 150px;">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_23.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_23.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_24.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_24.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_25.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_25.content')}}</p>
                </div>
            </div>
        </div>
    </gameslide>
    @include('components.footer')
</div>
    </gameslide>
    <div style="margin-bottom: 250px;"></div>
    @include('components.footer')
</div>
@endsection
