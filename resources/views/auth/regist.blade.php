<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Boosting Service</title>

	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="particles.js is a lightweight JavaScript library for creating particles.">
    <meta name="author" content="Vincent Garreau" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="css/particle.css">
    <link rel="stylesheet" href="assets/css/style2.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
    <link href="./assets/js/jquery-ui.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    
</head>

<body>


<div class="boxed_wrapper">

    <reg-home>

        <div class="">
            <div class="">
            </div>
            <div class="reg-mod-aling">
                <div class="join-aling">
                    <p>Join to us!</p>
                </div>
                <div class="reg-mod">           
                    <div class="reg-modal-log">
                        <p>Email</p>
                        <input type="email" id="" value="" name="email" placeholder="Email" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Name</p>
                        <input type="text" id="" name="name" value="" placeholder="Name" autocomplete="off">
                    </div>  
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password" placeholder="Password" autocomplete="off">
                    </div>
                    <div class="reg-modal-log">
                        <p>Password</p>
                        <input type="password" id="" name="password_confirmation" placeholder="Password confirm" autocomplete="off">
                        <input type="hidden" name="role" value="0">
                    </div>
                    
                    <button class="reg-modal-log-login">Registration</button>
                </div>
                <img src="./assets/images/logocom.gif" class="logo-com2">
                <div class="b-home">
                    <a href="">Back to Home</a>
                </div>
            </div>
        </div>

    </reg-home>

</div> 



<script src="./assets/js/external/jquery/jquery.js"></script>
<script src="./assets/js/jquery-ui.js"></script>
<script src="polzunok.js"></script>




</body>
</html>