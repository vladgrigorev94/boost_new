<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    use HasFactory;

    public $table = 'platforms';

    public function tarifs() {
        return $this->hasMany(\App\Models\Tarif::class);
    }
}
