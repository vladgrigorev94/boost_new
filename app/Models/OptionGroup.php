<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionGroup extends Model
{
    use HasFactory;

    public $table = 'option_groups';

    public function tarif() {
        return $this->belongsTo(\App\Models\Tarif::class);
    }

    public function options() {
        return $this->hasMany(\App\Models\Option::class, 'group_id', 'id');
    }
}
