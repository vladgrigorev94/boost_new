<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderOption extends Model
{
    use HasFactory;

    public $table = 'orders_options';

    // public function option() {
    //     return $this->belongsTo(\App\Models\Option::class);
    // }
}
