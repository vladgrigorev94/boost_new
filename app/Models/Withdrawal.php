<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILURE = 2;

    public function user() {
        return $this->belongsTo(\App\Models\User::class);
    }
}
