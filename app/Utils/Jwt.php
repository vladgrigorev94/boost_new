<?php namespace App\Utils;

use \Lcobucci\JWT\Builder;
use \Lcobucci\JWT\Signer\Hmac\Sha512;
use \Lcobucci\JWT\Signer\Key;

class Jwt
{
    static function token(){
        $time = time();
        $token = (new Builder())
            ->setIssuedAt($time) // Configures the time that the token was issue (iat claim)
            ->setExpiration($time + 3600) // Configures the expiration time of the token (exp claim)
            ->setSubject('250907213813', true) // Configures a new claim, called "sub" ( default subject of the JWT )
            ->getToken(new Sha512(), new Key('@w-eJ*2!NJXgSQv8YpC@yyYBUxk&SR6tYx!fa%*e@J&8ETcUuBeq-fd7VJN5zJz6')); // Retrieves the generated token

        return (string)$token;
    }

}