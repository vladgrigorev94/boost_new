<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;

class Options extends Section implements Initializable
{
    /**
     * @var \App\Role
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-group';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Options';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                [
                    AdminColumn::text('id', 'ID')->setWidth('30px'),
                    AdminColumn::text('name', 'Name')->setWidth('30px'),
                    AdminColumn::text('amount', 'Amount')->setWidth('30px'),
                    AdminColumn::text('group.name', 'Group')->setWidth('30px'),
                    AdminColumn::text('group.tarif.game.name', 'Game')->setWidth('30px'),
                    AdminColumn::text('group.tarif.platform.name', 'Platform')->setWidth('30px'),

                ]
            )->setApply(function ($query) {
                $query->orderBy('id', 'DESC');
            })->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function ($id) {
            $tabs = [];

            $tabs[] = AdminDisplay::tab(new FormElements([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::number('amount', 'Amount')->required(),
                AdminFormElement::number('bonus_cost', 'Bonus (cost)')->required(),

                AdminFormElement::number('bonus', 'Bonus (take)')->required(),
                AdminFormElement::select('group_id', 'Group', \App\Models\OptionGroup::class)
                    ->setDisplay('name'),
            ]))->setLabel('Common');

            return $tabs;
        });

        $form = AdminForm::panel()
            ->addHeader([
                $tabs
            ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function getCreateTitle()
    {
        return 'Add';
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }

    public function isCreatable()
    {
        return true;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
}