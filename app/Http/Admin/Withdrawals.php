<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;

class Withdrawals extends Section implements Initializable
{
    /**
     * @var \App\Role
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-group';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Withdrawals';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                [
                    AdminColumn::text('id', 'ID')->setWidth('30px'),
                    AdminColumn::text('user.name', 'User Name')->setWidth('30px'),
                    AdminColumn::text('user.email', 'User Email')->setWidth('30px'),
                    AdminColumn::text('user.id', 'User Id')->setWidth('30px'),
                    AdminColumn::text('amount', 'Sum')->setWidth('30px'),
                    AdminColumn::text('payment_system', 'Method')->setWidth('30px'),
                    AdminColumn::text('payment_info', 'Info')->setWidth('30px'),
                ]
            )->setApply(function ($query) {
                $query->orderBy('id', 'DESC');
            })->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function ($id) {
            $tabs = [];

            $tabs[] = AdminDisplay::tab(new FormElements([
                AdminFormElement::select('user_id', 'Product', \App\Models\User::class)
                    ->setDisplay('name')->required(),
                AdminFormElement::text('amount', 'Sum')
                    ->required(),
                AdminFormElement::text('payment_system', 'Method')
                    ->required(),
                AdminFormElement::text('payment_info', 'Info')
                    ->required(),
                AdminFormElement::image('screenshot', 'Screenshot'),
                AdminFormElement::textarea('comment', 'Comment'),
                AdminFormElement::select('status', 'Status', [
                    \App\Models\Withdrawal::STATUS_CREATED => 'created',
                    \App\Models\Withdrawal::STATUS_SUCCESS => 'success',
                    \App\Models\Withdrawal::STATUS_FAILURE => 'failure',
                ]),

            ]))->setLabel('Common');

            return $tabs;
        });

        $form = AdminForm::panel()
            ->addHeader([
                $tabs
            ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function getCreateTitle()
    {
        return 'Add';
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }

    public function isCreatable()
    {
        return false;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
}