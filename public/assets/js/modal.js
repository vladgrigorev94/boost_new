$(function() {
  $(document).on('click', '[id^="clickme-"]', function(event) {
      event.preventDefault();
      var parent = $(this).parent();
      var list = parent.find('.content2');
      if(list.css('display') === 'block') {
          list.hide();
      } else {
          list.show();
      }
  });
})